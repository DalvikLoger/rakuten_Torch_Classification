import pandas as pd
import numpy as np
import collections
import cv2 
import datetime
import hashlib
import inspect
import pickle
import gzip
import time
import tqdm
import html
import sys
import os
import re
import spacy
import sklearn
import nltk
from nltk.corpus import stopwords

from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline, make_pipeline
from sklearn.model_selection import train_test_split
from sklearn.feature_selection import SelectFromModel
from sklearn.metrics import f1_score, classification_report
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.svm import LinearSVC

import tensorflow as tf

from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Dense, Dropout, GlobalAveragePooling1D,\
                                    Input, Embedding, GRU, Bidirectional, \
                                    Conv2D, MaxPooling2D, GlobalMaxPooling2D, \
                                    BatchNormalization, concatenate, MaxPool2D , Flatten
                                    
X_test = pd.read_csv('X_test_gDTIJPh.csv', header=0, index_col=0)
X_train = pd.read_csv('X_train_12tkObq.csv', header=0, index_col=0)

labels_list = []
for i in y_train['color_tags']:
    label = []
    label.append(i)
    labels_list.append(label)
y_train['label'] = labels_list


X_train['label'] = y_train['label']

X_train = X_train.drop(['item_name','item_caption'], axis=1)
X_train = X_train.rename(columns = {'image_file_name' : 'filename'})
X_test = X_test.drop(['item_name','item_caption'], axis=1)
X_test = X_test.rename(columns = {'image_file_name' : 'filename'})
from keras.preprocessing.image import ImageDataGenerator
from pathlib import Path

img_gen = ImageDataGenerator(validation_split=0.2)
images_dir = Path('images')

# Convert 'filename' column to absolute paths
X_train['filename'] = X_train['filename'].apply(lambda x: str(images_dir / x))
X_test['filename'] = X_test['filename'].apply(lambda x: str(images_dir / x))

import ast
X_train = X_train.explode('label')
X_train['label'] = X_train['label'].apply(ast.literal_eval)

# Explode the DataFrame
X_train_exploded = X_train.explode('label')

img_gen = ImageDataGenerator(rescale=1./255, validation_split=0.2)
img_iter = img_gen.flow_from_dataframe(
    X_train,
    shuffle=True,
    x_col='filename',
    y_col='label',
    class_mode='categorical',
    target_size=(224, 224),
    batch_size=100,
    subset='training'
)

img_iter_val = img_gen.flow_from_dataframe(
    X_train,
    shuffle=False,
    x_col='filename',
    y_col='label',
    class_mode='categorical',
    target_size=(224, 224),
    batch_size=100,
    subset='validation'
)

from tensorflow.keras.applications import ConvNeXtBase, ResNet50, NASNetLarge, VGG19
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, GlobalAveragePooling2D, GaussianDropout, Reshape, SeparableConv2D, GlobalMaxPooling2D
from tensorflow_addons.optimizers import AdamW
base_model = NASNetLarge(weights='imagenet', include_top=False, input_shape=(224, 224, 3))

for layer in base_model.layers:
    layer.trainable = False

model = Sequential()

# Add the base model to the sequential model
model.add(base_model)

# Additional convolutional layers
model.add(Conv2D(filters=224, kernel_size=(3, 3), padding="same", activation="relu"))
model.add(Conv2D(filters=224, kernel_size=(3, 3), padding="same", activation="relu"))
model.add(Conv2D(filters=224, kernel_size=(3, 3), padding="same", activation="relu"))
model.add(BatchNormalization())
model.add(MaxPool2D())

# Additional convolutional layers
model.add(Conv2D(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
model.add(Conv2D(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
model.add(Conv2D(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
model.add(BatchNormalization())
model.add(MaxPool2D())

# Additional convolutional layers
model.add(SeparableConv2D(filters=1024, kernel_size=(3, 3), padding="same", activation="relu"))
model.add(SeparableConv2D(filters=1024, kernel_size=(3, 3), padding="same", activation="relu"))
model.add(SeparableConv2D(filters=1024, kernel_size=(3, 3), padding="same", activation="relu"))
model.add(BatchNormalization())
model.add(GlobalMaxPooling2D())

# Fully connected layers
model.add(Dense(units=512, activation="relu"))
model.add(BatchNormalization())
model.add(Dense(units=256, activation="relu"))
model.add(BatchNormalization())
# Output layer
model.add(Dense(units=19, activation="sigmoid"))
optimizer = AdamW(learning_rate=0.001, weight_decay=1e-4)
model.compile(optimizer=optimizer, loss='binary_crossentropy', metrics=['accuracy'])
model.summary()

from tensorflow.keras.callbacks import EarlyStopping
early = EarlyStopping(monitor='val_accuracy', min_delta=0, patience=20, verbose=1, mode='auto')

hist = model.fit(img_iter, validation_data=img_iter_val, 
                           epochs=50, 
                           steps_per_epoch=100, 
                           validation_steps=10, 
                           callbacks=[early])

model.save('NasNet_Rakuten.h5')
import matplotlib.pyplot as plt
plt.plot(hist.history['loss'])
plt.plot(hist.history['val_loss'])
plt.title("Loss curve")
plt.ylabel("Loss")
plt.xlabel("Epoch")
plt.legend(["loss","Validation Loss"])
plt.show()

import matplotlib.pyplot as plt
plt.plot(hist.history['accuracy'])
plt.plot(hist.history['val_accuracy'])
plt.title("Accuracy curve")
plt.ylabel("Accuracy")
plt.xlabel("Epoch")
plt.legend(["Accuracy","Validation Accuracy"])
plt.show()

test_datagen = ImageDataGenerator()
test_generator = test_datagen.flow_from_directory(
    X_test,
    target_size=(224, 224),
    batch_size=1,
    class_mode=None,
    shuffle=False
)

from keras.models import load_model


saved_model = load_model('NasNet_1.h5')

output = saved_model.predict_generator(test_generator, verbose=1, steps= len(test_generator))

predicted_class_indices=np.argmax(output,axis=1)
labels = (img_iter.class_indices)
labels = dict((v,k) for k,v in labels.items())
predictions = [labels[k] for k in predicted_class_indices]

filenames=test_generator.filenames
results=pd.DataFrame({"filename":filenames,
                      "Predictions":predictions})
