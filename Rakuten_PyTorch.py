import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torchvision
from torchvision import models,transforms,datasets
import numpy as np
import os
import time
from torch.utils.data import DataLoader
from torchvision.transforms import ToTensor
from torch.utils.data import Dataset
import matplotlib.pyplot as plt
import pandas as pd
import torch
from torch.optim import AdamW
import torchvision.transforms as transforms
from torch.utils.data import Dataset, DataLoader
from torchvision.models import resnet18
from PIL import Image

X_test = pd.read_csv('Rakuten_Challenge/X_test_gDTIJPh.csv', header=0,)
X_train = pd.read_csv('Rakuten_Challenge/X_train_12tkObq.csv', header=0, index_col=0)

labels_list = []
for i in y_train['color_tags']:
    label = []
    label.append(i)
    labels_list.append(label)
y_train['label'] = labels_list


X_train['label'] = y_train['label']

X_train = X_train.drop(['item_name','item_caption'], axis=1)
X_train = X_train.rename(columns = {'image_file_name' : 'filename'})
X_test = X_test.drop(['item_name','item_caption'], axis=1)
X_test = X_test.rename(columns = {'image_file_name' : 'filename'})
from keras.preprocessing.image import ImageDataGenerator
from pathlib import Path
import pandas as pd

img_gen = ImageDataGenerator(validation_split=0.2)
images_dir = Path('Rakuten_Challenge/images')

# Convert 'filename' column to absolute paths
X_train['filename'] = X_train['filename'].apply(lambda x: str(images_dir / x))
X_test['filename'] = X_test['filename'].apply(lambda x: str(images_dir / x))

class CustomDataset(Dataset):
    def __init__(self, dataframe, label_mapping, transform=None, target_transform=None):
        self.dataframe = dataframe
        self.transform = transform
        self.target_transform = target_transform
        self.label_mapping = label_mapping

    def __len__(self):
        return len(self.dataframe)

    def __getitem__(self, idx):
        img_path, labels = self.dataframe.iloc[idx]

        try:
            image = Image.open(img_path).convert("RGB")

            if self.transform:
                image = self.transform(image)

            # Encode the labels using the mapping
            labels = [self.label_mapping[label] for label in labels]

            if self.target_transform:
                labels = self.target_transform(labels)

            return image, labels

        except Exception as e:
            # Print the file path causing the error
            print(f"Error loading image at path: {img_path}")
            print(f"Error details: {e}")
            return None, None 
        
label_mapping = {'Silver': 0, 'Grey': 1, 'Black': 2, 'Brown': 3, 'White': 4, 'Yellow' : 5,'Orange' :6,
 'Pink' : 7,'Red':8, 'Beige' : 9, 'Gold':10, 'Purple':11, 'Green':12, 'Blue':13, 'Multiple Colors':14,
 'Navy':15, 'Khaki':16, 'Burgundy':17,'Transparent':18 }


def pad_labels(labels, num_classes):
    padded_labels = [0] * num_classes
    for label in labels:
        padded_labels[label] = 1
    return torch.tensor(padded_labels, dtype=torch.float32)

transform = transforms.Compose([
    transforms.Resize((224, 224)),
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
])

# 3. Model Definition
import torch.nn as nn
from torchvision.models import resnet152

class CustomCompactModel(nn.Module):
    def __init__(self, num_classes):
        super(CustomCompactModel, self).__init__()
        resnet = resnet152(weights='ResNet152_Weights.IMAGENET1K_V2')
        self.features = nn.Sequential(*list(resnet.children())[:-1])  # Remove the last fully connected layer
        self.fc = nn.Linear(2048, 2048)  # Corrected fully connected layer
        self.output = nn.Linear(2048, num_classes)

    def forward(self, x):
        x = self.features(x)
        x = x.view(x.size(0), -1)
        x = nn.functional.relu(self.fc(x))
        x = self.output(x)
        return x

    def predict_proba(self, x):
        # Apply softmax activation to get class probabilities
        return F.softmax(self.forward(x), dim=1)
    

target_transform = transforms.Lambda(lambda y: pad_labels(y, num_classes=19))
train_dataset = CustomDataset(X_train, label_mapping, transform=transform, target_transform=target_transform)
train_loader = DataLoader(train_dataset, batch_size=64, shuffle=True)

# Model, criterion, and optimizer
num_classes = 19
model = CustomCompactModel(num_classes)
criterion = nn.BCEWithLogitsLoss()
optimizer = torch.optim.AdamW(model.parameters(), lr=0.001)

# Number of epochs
num_epochs = 20

for epoch in range(num_epochs):
    total_loss = 0.0
    correct_predictions = 0
    total_samples = 0

    for inputs, labels in train_loader:
        # Check if inputs or labels are None (indicating an image loading error)
        if inputs is None or labels is None:
            continue

        optimizer.zero_grad()
        outputs = model(inputs)

        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()

        probabilities = torch.sigmoid(outputs)
        predictions = (probabilities > 0.5).float()  # Assuming binary classification for each class

        if predictions.size(1) != labels.size(1):
            raise ValueError("Number of classes in predictions and labels must match.")

        correct_predictions += torch.sum((predictions == labels).all(dim=1)).item()
        total_samples += labels.size(0)
        total_loss += loss.item()

    # Calculate average loss and accuracy for the epoch
    average_loss = total_loss / len(train_loader)
    accuracy = correct_predictions / total_samples

    # Print loss and accuracy after each epoch
    print(f'Epoch {epoch + 1}/{num_epochs}, Loss: {average_loss:.4f}, Accuracy: {accuracy * 100:.2f}%')


torch.save(model, 'resnet34_Rakuten.pth')