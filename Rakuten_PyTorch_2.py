import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torchvision
from torchvision import models,transforms,datasets
import numpy as np
import os
import time
from torch.utils.data import DataLoader
from torchvision.transforms import ToTensor
from torch.utils.data import Dataset
import matplotlib.pyplot as plt
import pandas as pd

X_test = pd.read_csv('Rakuten_Challenge/X_test_gDTIJPh.csv', header=0,)
X_train = pd.read_csv('Rakuten_Challenge/X_train_12tkObq.csv', header=0, index_col=0)

labels_list = []
for i in y_train['color_tags']:
    label = []
    label.append(i)
    labels_list.append(label)
y_train['label'] = labels_list


X_train['label'] = y_train['label']

X_train = X_train.drop(['item_name','item_caption'], axis=1)
X_train = X_train.rename(columns = {'image_file_name' : 'filename'})
X_test = X_test.drop(['item_name','item_caption'], axis=1)
X_test = X_test.rename(columns = {'image_file_name' : 'filename'})
from keras.preprocessing.image import ImageDataGenerator
from pathlib import Path
import pandas as pd

img_gen = ImageDataGenerator(validation_split=0.2)
images_dir = Path('Rakuten_Challenge/images')

# Convert 'filename' column to absolute paths
X_train['filename'] = X_train['filename'].apply(lambda x: str(images_dir / x))
X_test['filename'] = X_test['filename'].apply(lambda x: str(images_dir / x))

class CustomDataset(Dataset):
    def __init__(self, dataframe, transform=None):
        self.dataframe = dataframe
        self.transform = transform

    def __len__(self):
        return len(self.dataframe)

    def __getitem__(self, idx):
        img_path = self.dataframe.iloc[idx]['filename']  # Adjust 'img_path' to your actual column name

        try:
            image = Image.open(img_path).convert("RGB")

            if self.transform:
                image = self.transform(image)

            return image

        except Exception as e:
            print(f"Error loading image at path: {img_path}")
            print(f"Error details: {e}")
            return None 

# Assuming you have a similar structure for your test data
test_dataset = CustomDataset(X_test, transform=transform)
test_loader = DataLoader(test_dataset, batch_size=64, shuffle=False) # No need to shuffle test data

num_classes = 19

# Load custom weights
model = torch.load('Rakuten_Challenge/resnet34_Rakuten.pth')

all_predictions = []
with torch.no_grad():
    for test_inputs in test_loader:
        test_outputs = model(test_inputs)
        probabilities = torch.sigmoid(test_outputs)
        all_predictions.append(probabilities)

all_predictions = torch.cat(all_predictions, dim=0)
predicted_classes = torch.argmax(all_predictions, dim=1)

label_mapping = {'Silver': 0, 'Grey': 1, 'Black': 2, 'Brown': 3, 'White': 4, 'Yellow': 5, 'Orange': 6,
                 'Pink': 7, 'Red': 8, 'Beige': 9, 'Gold': 10, 'Purple': 11, 'Green': 12, 'Blue': 13,
                 'Multiple Colors': 14, 'Navy': 15, 'Khaki': 16, 'Burgundy': 17, 'Transparent': 18}

threshold = 0.5
predicted_classes = (all_predictions > threshold).int()

# Convert the predicted classes to a list of labels
predicted_labels = []
for row in predicted_classes:
    # Assuming predicted_classes is a binary tensor
    labels = [key for key, value in label_mapping.items() if row[value].item() == 1]
    
    # If no 1 is found, assign the label 'Transparent'
    if not labels:
        labels = ['Black']

    predicted_labels.append(labels)

# Add the predicted_labels to your X_test DataFrame
X_test['labels'] = predicted_labels

X_test.head(20)